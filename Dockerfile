FROM alpine:3.6

# add our user and group first to make sure their IDs get assigned consistently, regardless of whatever dependencies get added
RUN addgroup -S disque && adduser -S -G disque disque

# grab su-exec for easy step-down from root
RUN apk add --no-cache 'su-exec>=0.2'

ENV DISQUE_VERSION 1.0-rc1
ENV DISQUE_DOWNLOAD_URL https://github.com/antirez/disque/archive/${DISQUE_VERSION}.tar.gz
ENV DISQUE_DOWNLOAD_SHA 2d6fc85d16c8009154fc24d7fb004708f864712853d417cbea74bb6c2694e134

RUN set -ex \
  \
  && apk add --no-cache --virtual .build-deps \
    coreutils \
    gcc \
    linux-headers \
    make \
    musl-dev \
    openssl \
  \
  && wget -O disque.tar.gz "$DISQUE_DOWNLOAD_URL" \
  && echo "$DISQUE_DOWNLOAD_SHA *disque.tar.gz" | sha256sum -c - \
  && mkdir -p /usr/src/disque \
  && tar -xzf disque.tar.gz -C /usr/src/disque --strip-components=1 \
  && rm disque.tar.gz \
  \
  && sed -i 's/HAVE_BACKTRACE/HAVE_BACKTRACE_DISABLE/' /usr/src/disque/src/config.h \
  \
  && make -C /usr/src/disque -j "$(nproc)" \
  && make -C /usr/src/disque install \
  \
  && rm -r /usr/src/disque \
  && apk del .build-deps

RUN mkdir /data && chown disque:disque /data
VOLUME /data
WORKDIR /data

COPY docker-entrypoint.sh /usr/local/bin/
ENTRYPOINT ["docker-entrypoint.sh"]

EXPOSE 7711
CMD ["disque-server"]
