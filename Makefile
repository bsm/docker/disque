IMAGE=blacksquaremedia/disque
VERSION=1.0-rc1

build:
	docker build -t $(IMAGE):$(VERSION) .

tag: build
	docker tag $(IMAGE):$(VERSION) $(IMAGE):latest

push: tag
	docker push $(IMAGE):$(VERSION)
	docker push $(IMAGE):latest

run:
	docker run -it $(IMAGE):$(VERSION) sh
